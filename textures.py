from PIL import Image
import cv2
import numpy as np
import os

class Textures:
    def __init__(self, save_path):
        self.save_path = save_path

    def load_img(self, path):
        im = Image.open(path)
        data = np.array(im)
        return data

    def tile(self, data, tiling):
        tiled = numpy.tile(data, (tiling, tiling, 1))
        tiled = cv2.resize(tiled, dsize=data.shape, interpolation=cv2.INTER_LINEAR)
        return tiled

    def combine_RGBA(self, RGBA, default_value=0.0):
        output = None
        for i in range(4):
            data = RGBA[i]
            if data:
                if not output:
                    output = np.zeros((data.shape[0], data.shape[1], 4), dtype=np.uint8)
                    output += default_value
                output[:, :, i] = data[:, :]
        return output

    def combine_RGBA_from_path(self, path_RGBA, default_value=0.0):
        output = None
        for i in range(4):
            path = path_RGBA[i]
            if path:
                data = self.load_img(path)
                if not output:
                    output = np.zeros((data.shape[0], data.shape[1], 4), dtype=np.uint8)
                    output += default_value
                if len(data.shape) > 1:
                    output[:, :, i] = data[:, :, 0]
                else:
                    output[:, :, i] = data[:, :]
        return output

    def save_RGBA_img(self, data, path):
        img = Image.fromarray(data, 'RGBA')
        img.save(os.path.join(self.save_path, path))

    def save_RGB_img(self, data, path):
        img = Image.fromarray(data, 'RGB')
        img.save(os.path.join(self.save_path, path))

    def save_gray_img(self, data, path):
        img = Image.fromarray(data, 'L')
        img.save(os.path.join(self.save_path, path))

class PBR:
    def __init__(self,
                size,
                albedo = None,
                tiling = 1,
                default_albedo = [0.0, 0.0, 0.0],
                normalmap = None,
                AO = None,
                default_AO = 1.0,
                metalic = None,
                default_metalic = 0.0,
                roughtness = None,
                default_roughtness = 0.0,
                invert_roughtness = True,
                height = None,
                default_height = 0.5,
                emission = None,
                default_emission = [0.0, 0.0, 0.0]):
        """PBR struc class to handle loading of a PBR texture set.
        - size: The output size of the texture (example: 256, 512, 1024, 2048, 4096).
        - tiling: The tiling of the PBR texture, default to 1 (must be an integer).
        - albedo: the base color or albedo of the texture as a RGB texture (if a RGBA is loaded, only RGB will be used).
        - default_albedo: default albedo value as a RGB is no image map is provided ([R, G, B] value from 0.0 to 1.0).
        - normalmap: a RGB normal map file where R and G contain the normal information.
        - AO: Ambient Oclusion data as a gray scale image or RGBA (if RGBA only the R channel will be used).
        - default_AO: default ambient oclusion value if no AO map is provided (float from 0 to 1).
        - height: height (displacement) data as a gray scale image or RGBA (if RGBA only the R channel will be used).
        - default_height: default height displacement value if no height map is provided (float from 0 to 1).
        - metalic: Metalic data as a gray scale image or RGBA (if RGBA only the R channel will be used).
        - default_metalic: default metalic value is no image map is provided (float from 0 to 1).
        - roughtness: Roughtness data as a gray scale image or RGBA (if RGBA only the R channel will be used).
        - default_roughtness: default roughtness value is no image map is provided (float from 0 to 1).
        - invert_roughtness: Neos by default uses Glossiness, therefore roughtness maps are inverted by default. Change to False if using a glossiness map.
        - emission: emission data as a RGB image (if RGBA only the RGB channel will be used).
        - default_emission: default emission value as a RGB is no image map is provided ([R, G, B] value from 0.0 to 1.0).
        """
        self.texture_handler = Textures(".")
        self.size = size

        self.albedo = np.zeros((self.size[0], self.size[1], 3), dtype=np.uint8)
        for i in range(3):
            self.albedo[:, :, i] = int(default_albedo[i] * 255)
        if albedo:
            self.albedo = self.texture_handler.load_img(albedo)

        self.normalmap = np.zeros((self.size[0], self.size[1], 2), dtype=np.uint8) + 128
        if normalmap:
            self.normalmap = self.texture_handler.load_img(normalmap)

        self.AO = np.zeros((self.size[0], self.size[1]), dtype=np.uint8) + int(default_AO * 255)
        if AO:
            self.AO = self.texture_handler.load_img(AO)

        self.height = np.zeros((self.size[0], self.size[1]), dtype=np.uint8) + int(default_height * 255)
        if height:
            self.height = self.texture_handler.load_img(height)

        self.metalic = np.zeros((self.size[0], self.size[1]), dtype=np.uint8) + int(default_metalic * 255)
        if metalic:
            self.metalic = self.texture_handler.load_img(metalic)

        self.roughtness = np.zeros((self.size[0], self.size[1]), dtype=np.uint8) + int(default_roughtness * 255)
        if roughtness:
            self.roughtness = self.texture_handler.load_img(roughtness)
        if invert_roughtness:
            self.roughtness = ((self.roughtness.astype(np.int16) - 255) * -1).astype(np.uint8)

        self.emission = np.zeros((self.size[0], self.size[1], 3), dtype=np.uint8)
        for i in range(3):
            self.emission[:, :, i] = int(default_emission[i] * 255)
        if emission:
            self.emission = self.texture_handler.load_img(emission)

        if type(tiling) != int:
            self.tiling = 1
            print("Tiling not an integer, defaulting to 1!")
        else:
            self.tiling = tiling
        # if self.tiling > 1:
        #     self.tile_textures()

    def tile_textures(self):
        "Perform the texture retiling on load."
        self.albedo = self.texture_handler.tile(self.albedo, self.tiling)
        self.normalmap = self.texture_handler.tile(self.normalmap, self.tiling)
        self.AO = self.texture_handler.tile(self.AO, self.tiling)
        self.height = self.texture_handler.tile(self.height, self.tiling)
        self.metalic = self.texture_handler.tile(self.metalic, self.tiling)
        self.roughtness = self.texture_handler.tile(self.roughtness, self.tiling)
        self.emission = self.texture_handler.tile(self.emission, self.tiling)

    def resize(self, size):
        "Perform a texture resizing of the PBR pack."
        self.albedo = cv2.resize(self.albedo, dsize=size, interpolation=cv2.INTER_LINEAR)
        self.normalmap = cv2.resize(self.normalmap, dsize=size, interpolation=cv2.INTER_LINEAR)
        self.AO = cv2.resize(self.AO, dsize=size, interpolation=cv2.INTER_LINEAR)
        self.height = cv2.resize(self.height, dsize=size, interpolation=cv2.INTER_LINEAR)
        self.metalic = cv2.resize(self.metalic, dsize=size, interpolation=cv2.INTER_LINEAR)
        self.roughtness = cv2.resize(self.roughtness, dsize=size, interpolation=cv2.INTER_LINEAR)
        self.emission = cv2.resize(self.emission, dsize=size, interpolation=cv2.INTER_LINEAR)
