import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GdkPixbuf

# from textures import PBR

# builder = Gtk.Builder()
# builder.add_from_file("interface.glade")
# window = builder.get_object("MainWindow")
# window.show_all()
# window.connect("destroy", Gtk.main_quit)
#
# PBR_Item = builder.get_object("PBR_Item")
#

(TARGET_ENTRY_TEXT, TARGET_ENTRY_PIXBUF) = range(2)
(COLUMN_TEXT, COLUMN_PIXBUF) = range(2)


class DragSourceIconView(Gtk.IconView):
    def __init__(self, item_type, database, main_interface):
        Gtk.IconView.__init__(self)
        self.set_text_column(COLUMN_TEXT)
        self.set_pixbuf_column(COLUMN_PIXBUF)
        self.set_activate_on_single_click(True)

        model = Gtk.ListStore(str, GdkPixbuf.Pixbuf)
        self.set_model(model)

        self.enable_model_drag_source(Gdk.ModifierType.BUTTON1_MASK, [], Gdk.DragAction.COPY)
        self.connect("drag-data-get", self.on_drag_data_get)
        self.connect("item-activated", self.on_item_activated)
        self.connect("key-press-event", self.on_key_pressed)

        self.item_type = item_type
        self.database = database
        self.main_interface = main_interface

        self.active_item = None
        self.active_iter = None

        self.build_items()

    def build_items(self):
        if self.item_type == "PBR":
            self.add_add_item("Add PBR material")
            for material in self.database.data["PBR_Materials"]:
                self.add_item(material)
        elif self.item_type == "Image":
            self.add_add_item("Add Image")
            for image in self.database.data["Images"]:
                self.add_item(image)

    def on_update_name(self, new_name):
        self.get_model().set_value(self.active_iter, COLUMN_TEXT, new_name)

    def on_update_icon(self, new_pixbuf):
        self.get_model().set_value(self.active_iter, COLUMN_PIXBUF, new_pixbuf)

    def on_key_pressed(self, widget, event):
        print("KEY PRESSED: ", event.keyval)
        if event.keyval in [65288, 65535]:
            print("Deleting element: ", self.active_item)

    def on_item_activated(self, widget, selected_path):
        self.active_iter = self.get_model().get_iter(selected_path)
        self.active_item = self.get_model().get_value(self.active_iter, COLUMN_TEXT)
        print("Active item: ", self.active_item)
        if self.item_type == "PBR":
            self.main_interface.on_material_selected(self.active_item)
        elif self.item_type == "Image":
            self.main_interface.on_image_selected(self.active_item)

    def on_drag_data_get(self, widget, drag_context, data, info, time):
        selected_path = self.get_selected_items()[0]
        selected_iter = self.get_model().get_iter(selected_path)

        text = self.get_model().get_value(selected_iter, COLUMN_TEXT)
        pixbuf = self.get_model().get_value(selected_iter, COLUMN_PIXBUF)

        print(text, pixbuf)

        data.set_text(text, -1)
        data.set_pixbuf(pixbuf)

    def add_add_item(self, text):
        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale("./image-add-icon.png", 50, 50, True)
        self.get_model().append([text, pixbuf])

    def add_item(self, item):
        if self.item_type == "PBR":
            if self.database.data["PBR_Materials"][item]["albedo"]:
                pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(self.database.data["PBR_Materials"][item]["albedo"], 50, 50, True)
            else:
                pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale("./image-missing-icon.png", 50, 50, True)
        elif self.item_type == "Image":
            if self.database.data["Images"][item]["path"]:
                pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(self.database.data["PBR_Materials"][item]["path"], 50, 50, True)
            else:
                pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale("./image-missing-icon.png", 50, 50, True)
        self.get_model().append([item, pixbuf])


class DropArea(Gtk.VBox):
    def __init__(self, accepted_item_type):
        Gtk.Box.__init__(self)

        self.accepted_item_type = accepted_item_type

        self.icon = Gtk.Image.new()
        self.label = Gtk.Label.new()
        self.label.set_label("Drop something on me!")
        self.add(self.icon)
        self.add(self.label)

        self.drag_dest_set(Gtk.DestDefaults.ALL, [], DRAG_ACTION)

        self.connect("drag-data-received", self.on_drag_data_received)

    def on_drag_data_received(self, widget, drag_context, x, y, data, info, time):
        text = data.get_text()
        print("Received text: %s" % text)
        self.label.set_label(text)


# class NeosShaderInterface(Gtk.VBox):
#     def __init__(self, database):


class Interface:
    def __init__(self, database):
        self.database = database
        builder = Gtk.Builder()
        builder.add_from_file("interface.glade")
        builder.connect_signals(self)
        self.window = builder.get_object("MainWindow")

        MaterialScrolledWindow = builder.get_object("MaterialScrolledWindow")
        self.MaterialIconView = DragSourceIconView("PBR", database, self)
        self.MaterialIconView.drag_source_set_target_list(None)
        self.MaterialIconView.drag_source_add_text_targets()
        MaterialScrolledWindow.add(self.MaterialIconView)

        ImageScrolledWindow = builder.get_object("ImageScrolledWindow")
        self.ImageIconView = DragSourceIconView("Image", database, self)
        self.ImageIconView.drag_source_set_target_list(None)
        self.ImageIconView.drag_source_add_text_targets()
        ImageScrolledWindow.add(self.ImageIconView)

        # self.drop_area = DropArea("PBR")
        # self.drop_area.drag_dest_set_target_list(None)
        # self.drop_area.drag_dest_add_text_targets()


        self.MaterialParameters = builder.get_object("MaterialParameters")
        self.console = builder.get_object("Console")
        #
        # self.ImageListFlowBox = builder.get_object("ImageListFlowBox")
        # self.ImageListFlowBox.connect("child-activated", self.on_image_selected)
        # self.read_and_build_image_list()

        self.window.show_all()
        self.window.connect("destroy", Gtk.main_quit)

        # Initializing global variables
        self.selected_item = None
        self.dragged_material = None
        self.main_output_folder = "."

    # ================================
    # Helper functions
    # ================================

    def run(self):
        Gtk.main()

    def error(self, label):
        self.console.set_text("ERROR: {}".format(label))

    def warning(self, label):
        self.console.set_text("WARNING: {}".format(label))

    def debug(self, label):
        self.console.set_text("DEBUG: {}".format(label))

    def save_item(self):
        if self.item_type == "PBR":
            self.database.data["PBR_Materials"][self.selected_item["name"]] = self.selected_item
        elif self.item_type == "Image":
            self.database.data["Images"][self.selected_item["name"]] = self.selected_item

    # ================================
    # Interface building functions
    # ================================

    # def read_and_build_image_list(self):
    #     if len(self.ImageListFlowBox.get_children()) > 0:
    #         for child in self.ImageListFlowBox.get_children():
    #             self.ImageListFlowBox.remove(child)
    #     self.ImageListFlowBox.add(self.build_add_image_box())
    #     for image_name in self.database.data["Images"]:
    #         image = self.database.data["Images"][image_name]
    #         self.ImageListFlowBox.add(self.build_image_box(image))
    #     self.window.show_all()
    #     self.window.queue_draw()

    def build_add_image_box(self):
        box = Gtk.VBox(spacing=10)
        events = Gtk.EventBox.new()
        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale("./image-add-icon.png", 50, 50, True)
        image = Gtk.Image.new_from_pixbuf(pixbuf)
        events.connect("button-press-event", self.on_new_image_clicked)
        events.add(image)
        label = Gtk.Label.new()
        label.set_text("Add Image")
        box.pack_start(events, True, True, 0)
        box.pack_start(label, True, True, 0)
        return box

    def build_image_box(self, image):
        box = Gtk.VBox(spacing=10)
        if image["path"]:
            pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(image["path"], 50, 50, True)
        else:
            pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale("./image-missing-icon.png", 50, 50, True)
        preview = Gtk.Image.new_from_pixbuf(pixbuf)
        label = Gtk.Label.new()
        label.set_text(image["name"])
        delete_button = Gtk.Button.new_with_label("Delete")
        delete_button.connect("clicked", self.on_image_delete_pressed, image["name"])
        box.pack_start(preview, True, True, 0)
        box.pack_start(label, True, True, 0)
        box.pack_start(delete_button, True, True, 0)
        return box

    # def read_and_build_material_list(self):
    #     if len(self.MaterialListFlowBox.get_children()) > 0:
    #         for child in self.MaterialListFlowBox.get_children():
    #             self.MaterialListFlowBox.remove(child)
    #     self.MaterialListFlowBox.add(self.build_add_material_box())
    #     for material_name in self.database.data["PBR_Materials"]:
    #         material = self.database.data["PBR_Materials"][material_name]
    #         self.MaterialListFlowBox.add(self.build_material_box(material))
    #         # child = self.MaterialListFlowBox.get_children()[-1]
    #         # child.drag_source_set_target_list(None)
    #         # child.connect("drag-data-get", self.on_material_drag_begin)
    #     self.window.show_all()
    #     self.window.queue_draw()
    #
    # def build_add_material_box(self):
    #     box = Gtk.VBox(spacing=10)
    #     events = Gtk.EventBox.new()
    #     pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale("./image-add-icon.png", 50, 50, True)
    #     image = Gtk.Image.new_from_pixbuf(pixbuf)
    #     events.connect("button-press-event", self.on_new_material_clicked)
    #     events.add(image)
    #     label = Gtk.Label.new()
    #     label.set_text("Add PBR material")
    #     box.pack_start(events, True, True, 0)
    #     box.pack_start(label, True, True, 0)
    #     return box
    #
    # def build_material_box(self, material):
    #     box = Gtk.VBox(spacing=10)
    #     if material["albedo"]:
    #         pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(material["albedo"], 50, 50, True)
    #     else:
    #         pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale("./image-missing-icon.png", 50, 50, True)
    #     image = Gtk.Image.new_from_pixbuf(pixbuf)
    #     label = Gtk.Label.new()
    #     label.set_text(material["name"])
    #     delete_button = Gtk.Button.new_with_label("Delete")
    #     delete_button.connect("clicked", self.on_material_delete_pressed, material["name"])
    #     box.pack_start(image, True, True, 0)
    #     box.pack_start(label, True, True, 0)
    #     box.pack_start(delete_button, True, True, 0)
    #     return box

    def build_select_texture_file(self, label_text, data):
        box = Gtk.VBox(spacing=5)

        label = Gtk.Label.new()
        label.set_text(label_text)

        events = Gtk.EventBox.new()
        events.connect("button-press-event", self.on_material_edit_image_click, data)
        if self.selected_item[data]:
            pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(self.selected_item[data], 50, 50, True)
        else:
            pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale("./image-missing-icon.png", 50, 50, True)
        image = Gtk.Image.new_from_pixbuf(pixbuf)
        events.add(image)

        box.pack_start(label, True, True, 0)
        box.pack_start(events, True, True, 0)
        return box

    def build_horizontal_scale_label(self, min, max, label_text, data):
        box = Gtk.VBox(spacing=5)
        scale = Gtk.Scale.new_with_range(Gtk.Orientation.HORIZONTAL, 0, 1, 0.01)
        scale.set_value(self.selected_item[data])
        scale.connect("value-changed", self.on_scale_change, data)
        label = Gtk.Label.new()
        label.set_text(label_text)
        box.pack_start(scale, True, True, 0)
        box.pack_start(label, True, True, 0)
        return box

    def build_color_picker(self, label_text, data):
        box = Gtk.VBox(spacing=5)
        button = Gtk.ColorButton.new()
        color = button.get_color()
        color.red = self.selected_item[data][0] * 256
        color.green = self.selected_item[data][1] * 256
        color.blue = self.selected_item[data][2] * 256
        button.set_color(color)
        button.connect("color-set", self.on_color_activated, data)
        label = Gtk.Label.new()
        label.set_text(label_text)
        box.pack_start(label, True, True, 0)
        box.pack_start(button, True, True, 0)
        return box

    def build_checkbox(self, default, label_text, data):
        box = Gtk.VBox(spacing=5)
        button = Gtk.CheckButton.new_with_label(label_text)
        button.set_active(self.selected_item[data])
        button.connect("toggled", self.on_checkbox_toggled, data)
        box.pack_start(button, True, True, 0)
        return box

    def build_entry(self, label_text, data):
        box = Gtk.HBox(spacing=5)
        entry = Gtk.Entry.new()
        entry.set_size_request(None, 50)
        entry.set_text(str(self.selected_item[data]))
        entry.connect("changed", self.on_integer_editable_changed, data)
        label = Gtk.Label.new()
        label.set_text(label_text)
        box.pack_start(entry, True, True, 0)
        box.pack_start(label, True, True, 0)
        return box

    def build_entry_name(self, label_text, data):
        box = Gtk.HBox(spacing=5)
        entry = Gtk.Entry.new()
        entry.set_text(str(self.selected_item[data]))
        entry.connect("changed", self.on_name_changed, data)
        label = Gtk.Label.new()
        label.set_text(label_text)
        box.pack_start(entry, True, True, 0)
        box.pack_start(label, True, True, 0)
        return box

    def build_material_edit_box(self):
        if len(self.MaterialParameters) > 1:
            self.MaterialParameters.remove(self.MaterialParameters.get_children()[-1])
        box = Gtk.VBox(homogeneous=False, spacing=5)
        box.pack_start(self.build_entry_name("Name", "name"), True, True, 0)
        box.pack_start(self.build_entry("Texture size", "size"), True, True, 0)
        box.pack_start(self.build_entry("Tiling factor", "tiling"), True, True, 0)
        box.pack_start(self.build_select_texture_file("Albedo", "albedo"), True, True, 0)
        box.pack_start(self.build_color_picker("Default albedo value", "default_albedo"), True, True, 0)
        box.pack_start(self.build_select_texture_file("Emissive", "emission"), True, True, 0)
        box.pack_start(self.build_color_picker("Default emissive value", "default_emission"), True, True, 0)
        box.pack_start(self.build_select_texture_file("Normal map", "normalmap"), True, True, 0)
        box.pack_start(self.build_select_texture_file("Ambient Oclusion", "AO"), True, True, 0)
        box.pack_start(self.build_horizontal_scale_label(0, 1, "Default AO value", "default_AO"), True, True, 0)
        box.pack_start(self.build_select_texture_file("Metalic", "metalic"), True, True, 0)
        box.pack_start(self.build_horizontal_scale_label(0, 1, "Default metalic value", "default_metalic"), True, True, 0)
        box.pack_start(self.build_select_texture_file("Roughtness", "roughtness"), True, True, 0)
        box.pack_start(self.build_horizontal_scale_label(0, 1, "Default roughtness value", "default_roughtness"), True, True, 0)
        box.pack_start(self.build_checkbox(True, "Invert roughtness", "invert_roughtness"), True, True, 0)
        self.MaterialParameters.pack_start(box, True, True, 0)
        self.window.show_all()
        self.window.queue_draw()

    def build_image_edit_box(self):
        if len(self.MaterialParameters) > 1:
            self.MaterialParameters.remove(self.MaterialParameters.get_children()[-1])
        box = Gtk.VBox(homogeneous=False, spacing=5)
        box.pack_start(self.build_entry_name("Name", "name"), True, True, 0)
        box.pack_start(self.build_select_texture_file("Image file", "path"), True, True, 0)
        self.MaterialParameters.pack_start(box, True, True, 0)
        self.window.show_all()
        self.window.queue_draw()

    def add_texture_filters(self, dialog):
        filter_images = Gtk.FileFilter()
        filter_images.set_name("Image files")
        filter_images.add_mime_type("image/*")
        dialog.add_filter(filter_images)

        filter_any = Gtk.FileFilter()
        filter_any.set_name("Any files")
        filter_any.add_pattern("*")
        dialog.add_filter(filter_any)

    # ================================
    # Callbacks
    # ================================

    def on_checkbox_toggled(self, button, data=None):
        state = button.get_active()
        self.selected_item[data] = state
        self.save_item()

    def on_integer_editable_changed(self, widget, data=None):
        text = widget.get_text()
        try:
            text = int(text)
            self.selected_item[data] = text
        except:
            self.error("Value is not an integer!")
        self.save_item()

    def on_name_changed(self, widget, data=None):
        text = widget.get_text()
        if self.item_type == "PBR":
            del(self.database.data["PBR_Materials"][self.selected_item["name"]])
            self.selected_item["name"] = text
            self.save_item()
            self.MaterialIconView.on_update_name(text)
        elif self.item_type == "Image":
            del(self.database.data["Images"][self.selected_item["name"]])
            self.selected_item["name"] = text
            self.save_item()
            self.ImageIconView.on_update_name(text)

    def on_material_edit_image_click(self, widget, event, data=None):
        dialog = Gtk.FileChooserDialog(
            title="Please choose a texture file", parent=self.window, action=Gtk.FileChooserAction.OPEN
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL,
            Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN,
            Gtk.ResponseType.OK,
        )

        self.add_texture_filters(dialog)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            filename = dialog.get_filename()
            self.debug("File selected: " + filename)
            self.selected_item[data] = filename
            self.save_item()
        elif response == Gtk.ResponseType.CANCEL:
            self.debug("Canceled")

        dialog.destroy()

        image = widget.get_children()[0]
        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(filename, 50, 50, True)
        image.set_from_pixbuf(pixbuf)
        if self.item_type == "PBR":
            if data == "albedo":
                self.MaterialIconView.on_update_icon(pixbuf)
        elif self.item_type == "Image":
            self.ImageIconView.on_update_icon(pixbuf)

    def on_color_activated(self, widget, data=None):
        color = widget.get_color()
        self.selected_item[data] = [int(color.red/256), int(color.green/256), int(color.blue/256)]
        self.save_item()

    def on_scale_change(self, widget, data=None):
        value = widget.get_value()
        self.selected_item[data] = value
        self.save_item()

    def on_new_image_clicked(self, widget, event, data=None):
        image = self.database.get_default_image()
        self.database.data["Images"][image["name"]] = image
        self.ImageIconView.add_item(image["name"])

    def on_image_selected(self, name):
        if name == "Add Image":
            self.on_new_image_clicked(None, None)
        else:
            self.selected_item = self.database.data["Images"][name]
            self.item_type = "Image"
            self.build_image_edit_box()

    def on_image_delete_pressed(self, button, data=None):
        if data in self.database.data["Images"]:
            del(self.database.data["Images"][data])
            self.debug("Image {} deleted".format(data))
        self.read_and_build_material_list()

    def on_new_material_clicked(self, widget, event, data=None):
        pbr = self.database.get_default_material()
        self.database.data["PBR_Materials"][pbr["name"]] = pbr
        self.MaterialIconView.add_item(pbr["name"])

    def on_material_selected(self, name):
        if name == "Add PBR material":
            self.on_new_material_clicked(None, None)
        else:
            self.selected_item = self.database.data["PBR_Materials"][name]
            self.item_type = "PBR"
            self.build_material_edit_box()

    def on_material_delete_pressed(self, button, data=None):
        if data in self.database.data["PBR_Materials"]:
            del(self.database.data["PBR_Materials"][data])
            print("material deleted")
        self.read_and_build_material_list()

    def on_choose_output_folder_pressed(self, button, data=None):
        dialog = Gtk.FileChooserDialog(
            title="Please choose an output folder", parent=self.window, action=Gtk.FileChooserAction.SELECT_FOLDER
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL,
            Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN,
            Gtk.ResponseType.OK,
        )

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            data = dialog.get_filename()
            print("Open clicked")
            print("File selected: " + data)
        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")

        dialog.destroy()

    def on_generate_texture_pressed(self, button, data=None):
        print("To be linked")

    def on_save_database_pressed(self, button, data=None):
        try:
            self.database.save()
            self.debug("Database saved!")
        except:
            self.error("Cannot save the material database!!!")
