#!/usr/bin/env python3

from interface import Interface
from database import Database

if __name__ == "__main__":
    D = Database()
    I = Interface(D)
    I.run()
