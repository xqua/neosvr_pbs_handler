import json
import os
from shaders import *

class Database:
    def __init__(self, path="./database.json"):
        self.path = path
        self.read()

        # REGISTER A NEW SHADER HERE
        # Make sure it is in the shaders.py file.

        self.shaders = {
            "PBR Metalic": PBR_Metalic,
            "PBR Splat Metalic": PBR_Splat_Metalic,
        }

    def read(self):
        if os.path.isfile(self.path):
            f = open(self.path)
            self.data = json.load(f)
            f.close()
        else:
            self.data = {
            "PBR_Materials": {},
            "Images": {}
            }
        return self.data

    def save(self):
        try:
            if os.path.isfile(self.path):
                os.rename(self.path, self.path+".bak")
            f = open(self.path, 'w')
            json.dump(self.data, f)
            f.close()
            return True
        except:
            print("Error saving the database !!!")
            return False

    def save_material(self, material):
        if material["name"] in self.data["PBR_Materials"]:
            print("Error: Material name is not unique!")
            return False
        self.data["PBR_Materials"][material["name"]] = material

    def get_default_material(self):
        name = "Default Material"
        c = 1
        while name in self.data["PBR_Materials"]:
            name = "Default Material {0:03d}".format(c)
            c += 1
        default_material = {
            "name": name,
            "size": 512,
            "albedo": None,
            "tiling": 1,
            "default_albedo": [0.0, 0.0, 0.0],
            "normalmap": None,
            "AO": None,
            "default_AO": 1.0,
            "metalic": None,
            "default_metalic": 0.0,
            "roughtness": None,
            "default_roughtness": 0.0,
            "invert_roughtness": True,
            "height": None,
            "default_height": 0.5,
            "emission": None,
            "default_emission": [0.0, 0.0, 0.0]
        }
        return default_material

    def get_default_image(self):
        name = "Default Image"
        c = 1
        while name in self.data["Images"]:
            name = "Default Image {0:03d}".format(c)
            c += 1
        default_image = {
            "name": name,
            "path": None
        }
        return default_image

    def get_material(self, material_name):
        material = self.data["PBR_Materials"][material_name]
        pbr = PBR(
            size = material["size"],
            albedo = material["albedo"],
            tiling = material["tiling"],
            default_albedo = material["default_albedo"],
            normalmap = material["normalmap"],
            AO = material["AO"],
            default_AO = material["default_AO"],
            metalic = material["metalic"],
            default_metalic = material["default_metalic"],
            roughtness = material["roughtness"],
            default_roughtness = material["default_roughtness"],
            invert_roughtness = material["invert_roughtness"],
            height = material["height"],
            default_height = material["default_height"],
            emission = material["emission"],
            default_emission = material["default_emission"]
        )
        return pbr
