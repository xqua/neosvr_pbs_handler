from textures import Textures, PBR

class PBR_Metalic:
    def __init__(self, save_path, size, PBR):
        self.texture_handler = Textures(save_path)
        self.size = size
        PBR.resize(self.size)

    def make_maps(self):
        self.texture_handler.save_RGB_img(PBR.albedo, "AlbedoColor.png")
        self.texture_handler.save_RGB_img(PBR.normalmap, "NormalMap.png")
        self.texture_handler.save_RGB_img(PBR.emission, "EmissiveColor.png")
        self.texture_handler.save_gray_img(PBR.AO, "OcclusionMap.png")
        self.texture_handler.save_gray_img(PBR.height, "HeightMap.png")
        metalic_map_data = self.texture_handler.combine_RGBA([PBR.metalic, PBR.metalic, PBR.metalic, PBR.roughtness])
        self.texture_handler.save_RGBA_img(metalic_map_data, "MetalicMap.png")

class PBR_Splat_Metalic:
    def __init__(self, save_path, database):
        self.texture_handler = Textures(save_path)
        self.database = database

    def get_parameters(self):
        return [
            ["Colormap output size", "Entry"],
            ["Materials output size", "Entry"],
            ["ColorMap 1", "Image"],
            ["ColorMap 2", "Image"],
            ["ColorMap 3", "Image"],
            ["ColorMap 4", "Image"],
            ["Material 1", "PBR"],
            ["Material 2", "PBR"],
            ["Material 3", "PBR"],
            ["Material 4", "PBR"],
        ]

    def set(self, parameters):
        self.size = parameters[0]
        self.color_map_size = parameters[1]

        self.maps = []
        for map in [parameters[2], parameters[3], parameters[4], parameters[5]]:
            if map:
                self.maps.append(map)
            else:
                self.maps.append(np.zeros(self.color_map_size), dtype=np.uint8)

        self.textures = []
        for tex in [parameters[6], parameters[7], parameters[8], parameters[9]]:
            if tex:
                pbr = self.database.get_material(tex)
                self.textures.append(pbr)
            else:
                self.texture.append(PBR(self.size))
        [texture.resize(self.size) for texture in self.textures]

    def make_maps(self):
        self.make_colormap()
        self.make_normalmap()
        self.make_metalicmap()
        self.resave_textures()

    def resave_textures(self):
        for i in range(4):
            self.texture_handler.save_RGB_img(texture.albedo, "Albedo_{}.png".format(i))
            self.texture_handler.save_RGB_img(texture.emission, "Emission_{}.png".format(i))

    def make_colormap(self):
        """This function creates a RGBA map from 4 gray level color maps. Each map corresponds to a texture in the splat shader.
        map_1 is mandatory, but the other maps are optional. """
        color_map_data = self.texture_handler.combine_RGBA_from_path(self.maps)
        self.texture_handler.save_RGBA_img(color_map_data, "ColorMap.png")

    def make_normalmap(self):
        """This function creates two RGBA images resulting from the combined four NormalMaps.
        NormalMap R and G data are repacked onto the RG and BA channel of each image."""
        normal_map_12_data = self.texture_handler.combine_RGBA([self.textures[0].normalmap[0], self.textures[0].normalmap[1], self.textures[1].normalmap[0], self.textures[1].normalmap[1]])
        normal_map_34_data = self.texture_handler.combine_RGBA([self.textures[2].normalmap[0], self.textures[2].normalmap[1], self.textures[3].normalmap[0], self.textures[3].normalmap[1]])
        self.texture_handler.save_RGBA_img(normal_map_12_data, "NormalMap_12.png")
        self.texture_handler.save_RGBA_img(normal_map_34_data, "NormalMap_34.png")

    def make_metalicmap(self, invert=True):
        """This function creates two RGBA images resulting from the combined four metalic map and four roughtness maps.
        Metalic are repacked onto the R and B channels.
        Roughtness are repacked onto the G and A channels.
        Roughtness is inverted to Glossiness by default (Neos default is Glossiness).
        base_metalic is the default metalic value.
        base_roughness is the default roughtness value."""
        metalic_map_12_data = self.texture_handler.combine_RGBA([self.textures[0].metalic, self.textures[0].roughtness, self.textures[1].metalic, self.textures[1].roughtness])
        metalic_map_34_data = self.texture_handler.combine_RGBA([self.textures[2].metalic, self.textures[2].roughtness, self.textures[3].metalic, self.textures[3].roughtness])
        self.texture_handler.save_RGBA_img(metalic_map_12_data, "MetalicMap_12.png")
        self.texture_handler.save_RGBA_img(metalic_map_34_data, "MetalicMap_34.png")
